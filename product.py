# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Template']


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    # Is the repair service offered to the customer
    fix_product = fields.Many2One('product.template', 'Fix product',
        domain=[('type', '=', 'service')])
    fixable_products = fields.One2Many(
        'product.template', 'fix_product', 'Fixable products',
        domain=[('type', 'in', ['goods', 'assets'])],
        states={'invisible': Eval('type') != 'service'})

    @classmethod
    def copy(cls, products, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('fixable_products', None)
        default.setdefault('fix_product', None)
        return super().copy(products, default=default)

    @classmethod
    def view_attributes(cls):
        return super(Template, cls).view_attributes() + [
            ('//page[@id="fixable"]', 'states', {
                    'invisible': (Eval('type') != 'service')
                    })]
