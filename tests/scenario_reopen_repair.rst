=============================
Reopen Repairs Times Scenario
=============================

Imports::

    >>> from proteus import config, Model, Wizard
    >>> from decimal import Decimal
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_chart, get_accounts, create_tax
    >>> from datetime import timedelta
    >>> from trytond.exceptions import UserWarning

Activate modules::

    >>> config = activate_modules('electrans_repair')


Get models::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> Product = Model.get('product.product')
    >>> Repair = Model.get('repair.repair')
    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> User = Model.get('res.user')
    >>> Start = Model.get('repair.create.start')
    >>> ShipmentOutReturn = Model.get('stock.shipment.out.return')
    >>> Move = Model.get('stock.move')
    >>> Group = Model.get('res.group')
    >>> Address = Model.get('party.address')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()
    >>> other_tax = create_tax(Decimal('.5'))
    >>> other_tax.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Bienes")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> account_category.save()

Create sale category::

    >>> SaleCategory = Model.get('sale.category')
    >>> sale_category = SaleCategory(name="Ferroviaria")
    >>> sale_category.save()

Create Country::

    >>> Country = Model.get('country.country')
    >>> country = Country()
    >>> country.code = 'ES'
    >>> country.name = 'España'
    >>> country.save()

Create fix_product account category::

    >>> fix_account_category = ProductCategory(name="Servicios")
    >>> fix_account_category.accounting = True
    >>> fix_account_category.account_expense = expense
    >>> fix_account_category.account_revenue = revenue
    >>> fix_account_category.customer_taxes.append(other_tax)
    >>> fix_account_category.save()

Create repair user::

    >>> repair_user = User()
    >>> stock_group, = Group.find([('name', '=', 'Stock')])
    >>> repair_user.groups.append(stock_group)
    >>> repair_user.name = 'RepairUser'
    >>> repair_user.login = 'repair'
    >>> stock_admin_group, = Group.find([('name', '=', 'Stock Administration')])
    >>> product_admin_group, = Group.find([('name', '=', 'Product Administration')])
    >>> sales_admin_group, = Group.find([('name', '=', 'Sales Administrator')])
    >>> sales_to_quote_group, = Group.find([('name', '=', 'Sales to quote')])
    >>> work_project_admin_group, = Group.find([('name', '=', 'Work Project Administration')])
    >>> repair_user.groups.append(sales_to_quote_group)
    >>> repair_user.groups.append(stock_admin_group)
    >>> repair_user.groups.append(product_admin_group)
    >>> repair_user.groups.append(sales_admin_group)
    >>> repair_user.groups.append(work_project_admin_group)
    >>> repair_user.save()
    >>> config.user = repair_user.id

Create party::

    >>> Lang = Model.get('ir.lang')
    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.lang, = Lang.find([('code', '=', 'en')])
    >>> party.save()
    >>> address, = party.addresses
    >>> address.street = "St sample, 15"
    >>> address.city = "City"
    >>> address.invoice = True
    >>> address.save()

Create Project::

    >>> Project = Model.get('work.project')
    >>> project = Project()
    >>> project.company = company
    >>> project.category = sale_category
    >>> project.party = party
    >>> project.number = 'OFR00001'
    >>> project.save()

Create fix_product::

    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> fix_product = ProductTemplate()
    >>> fix_product.name = 'Fix  Product'
    >>> fix_product.default_uom = unit
    >>> fix_product.type = 'service'
    >>> fix_product.list_price = Decimal('10')
    >>> fix_product.cost_price = Decimal('5')
    >>> fix_product.cost_price_method = 'fixed'
    >>> fix_product.account_category = fix_account_category
    >>> fix_product.salable = True
    >>> fix_product.save()

Create products::

    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products
    >>> product.save()
    >>> template2 = ProductTemplate()
    >>> template2.name = 'product2'
    >>> template2.default_uom = unit
    >>> template2.type = 'goods'
    >>> template2.salable = True
    >>> template2.list_price = Decimal('10')
    >>> template2.cost_price = Decimal('5')
    >>> template2.cost_price_method = 'fixed'
    >>> template2.account_category = account_category
    >>> template2.save()
    >>> product2, = template2.products
    >>> product2.save()
    >>> template3 = ProductTemplate()
    >>> template3.name = 'product3'
    >>> template3.default_uom = unit
    >>> template3.type = 'goods'
    >>> template3.salable = True
    >>> template3.list_price = Decimal('10')
    >>> template3.cost_price = Decimal('5')
    >>> template3.cost_price_method = 'fixed'
    >>> template3.account_category = account_category
    >>> template3.save()
    >>> product3, = template3.products
    >>> product3.save()

Create services for irreparable and not damaged products::

    >>> template_irreparable = ProductTemplate()
    >>> template_irreparable.name = 'Irreparable'
    >>> template_irreparable.default_uom = unit
    >>> template_irreparable.type = 'service'
    >>> template_irreparable.salable = True
    >>> template_irreparable.list_price = Decimal('10')
    >>> template_irreparable.cost_price = Decimal('5')
    >>> template_irreparable.cost_price_method = 'fixed'
    >>> template_irreparable.account_category = fix_account_category
    >>> template_irreparable.salable = True
    >>> template_irreparable.save()
    >>> product_irreparable, = template_irreparable.products
    >>> product_irreparable.save()
    >>> template_not_damaged = ProductTemplate()
    >>> template_not_damaged.name = 'Not damaged'
    >>> template_not_damaged.default_uom = unit
    >>> template_not_damaged.type = 'service'
    >>> template_not_damaged.salable = True
    >>> template_not_damaged.list_price = Decimal('10')
    >>> template_not_damaged.cost_price = Decimal('5')
    >>> template_not_damaged.cost_price_method = 'fixed'
    >>> template_not_damaged.account_category = fix_account_category
    >>> template_not_damaged.salable = True
    >>> template_not_damaged.save()
    >>> product_not_damaged, = template_not_damaged.products
    >>> product_not_damaged.save()

Configure Repair::

    >>> Sequence = Model.get('ir.sequence')
    >>> Location = Model.get('stock.location')
    >>> RepairConfig = Model.get('repair.configuration')
    >>> repair_config = RepairConfig(1)
    >>> warehouse, = Location.find([('code', '=', 'WH')])
    >>> repairlocation = Location()
    >>> repairlocation.parent = warehouse
    >>> repairlocation.type = 'storage'
    >>> repairlocation.code = '12345'
    >>> repairlocation.name = 'Repair Location'
    >>> repairlocation.save()
    >>> repair_config.repair_location = repairlocation
    >>> repair_sequence, = Sequence.find([('name', '=', 'Repair Sequence')])
    >>> repair_config.repair_sequence = repair_sequence
    >>> repair_config.sale_title = 'REPAIRS VALORATION {{ record.number }}'
    >>> repair_config.max_time_assessment = timedelta(days = 14)
    >>> repair_config.max_time_fixing = timedelta(days = 28)
    >>> repair_config.product_category = account_category
    >>> repair_config.irreparable_assessment_service = product_irreparable
    >>> repair_config.not_damaged_assessment_service = product_not_damaged
    >>> repair_config.save()

Create Repair Order::

    >>> repair_order = Repair()
    >>> repair_order.product = product
    >>> repair_order.unknown_lot = True
    >>> repair_order2 = Repair()
    >>> repair_order2.product = product2
    >>> repair_order2.unknown_lot = True
    >>> repair_order3 = Repair()
    >>> repair_order3.product = product3
    >>> repair_order3.unknown_lot = True
    >>> repair = Wizard('repair.create')
    >>> repair.form.party = party
    >>> repair.form.repair_orders.append(repair_order)
    >>> repair.form.repair_orders.append(repair_order2)
    >>> repair.form.repair_orders.append(repair_order3)
    >>> try:
    ...   repair.execute('create_')
    ... except UserWarning as warning:
    ...   _, (key, *_) = warning.args
    ...   raise  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
       ...
    UserWarning: ...
    >>> Warning = Model.get('res.user.warning')
    >>> Warning(user=config.user, name=key).save()
    >>> repair.execute('create_')
    >>> shipment, = repair.actions[0]
    >>> shipment.save()
    >>> repairorder = Repair.find([])
    >>> shipment.click('receive')
    >>> shipment.state
    'received'
    >>> repairorder[0].click('wait_to_fix') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> shipment.click('done')

Change assessment(and create sale lines)::

    >>> repairorder[0].state
    'waiting_for_assessment'
    >>> repairorder[1].state
    'waiting_for_assessment'
    >>> repairorder[2].state
    'waiting_for_assessment'
    >>> reopen = Wizard('repair.reopen', [repairorder[0]]) # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> repairorder[0].click('wait_to_fix') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> repairorder[0].assessment = 'reparable'
    >>> repairorder[0].save()
    >>> repairorder[1].assessment = 'irreparable'
    >>> repairorder[1].save()
    >>> repairorder[2].assessment = 'not_damaged'
    >>> repairorder[2].save()
    >>> repairorder[0].click('wait_to_fix') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> template.fix_product = fix_product
    >>> template.save()
    >>> template2.fix_product = fix_product
    >>> template2.save()
    >>> template3.fix_product = fix_product
    >>> template3.save()
    >>> repairorder[0].click('wait_to_fix')
    >>> repairorder[0].save()
    >>> repairorder[0].state
    'waiting_to_fix'
    >>> repairorder[1].click('do')
    >>> repairorder[1].save()
    >>> repairorder[1].state
    'done'
    >>> repairorder[2].click('do')
    >>> repairorder[2].save()
    >>> repairorder[2].state
    'done'

Configure sale::

    >>> SaleConfig = Model.get('sale.configuration')
    >>> SequenceType = Model.get('ir.sequence.type')
    >>> sale_config = SaleConfig(1)
    >>> sale_sequence, = Sequence.find([('name', '=', 'Sale')])
    >>> sale_quotation_sequence, = Sequence.find([('name', '=', 'Sale Quotation')])
    >>> sale_config.sale_sequence = sale_sequence
    >>> sale_config.sale_quotation_sequence = sale_quotation_sequence
    >>> sale_config.save()

Reopen Repairs::

    >>> repairorder[0].sale.in_force_period = timedelta(days=120)
    >>> repairorder[0].sale.country = country
    >>> repairorder[0].sale.category = sale_category
    >>> repairorder[0].sale.project = project
    >>> repairorder[0].sale.save()
    >>> repairorder[0].sale.click('quote')
    >>> repairorder[0].sale.save()
    >>> repairorder[0].sale.state
    'quotation'
    >>> reopen = Wizard('repair.reopen', [repairorder[0]]) # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> repairorder[0].sale.click('draft')
    >>> repairorder[0].sale.save()
    >>> repairorder[0].sale.state
    'draft'
    >>> reopen = Wizard('repair.reopen', [repairorder[0]])
    >>> repairorder[0].assessment == ''
    True
    >>> repairorder[0].state == 'waiting_for_assessment'
    True

Finalize sale::

    >>> repairorder[0].assessment = 'reparable'
    >>> repairorder[0].save()
    >>> repairorder[0].click('wait_to_fix')
    >>> repairorder[0].save()
    >>> repairorder[0].sale.click('quote')
    >>> repairorder[0].sale.save()
    >>> repairorder[0].sale.party.allowed_for_sales = True
    >>> repairorder[0].sale.party.save()
    >>> create_order_wizard = Wizard('sale.create_order', [repairorder[0].sale])
    >>> create_order_wizard.execute('create_')
    >>> order, = create_order_wizard.actions[0]
    >>> order.invoice_address = address
    >>> order.click('confirm')
    >>> order.save()
    >>> order.state
    'processing'
    >>> order.click('process')
    >>> order.save()
    >>> order.state
    'processing'
    >>> repairorder[0].click('do')
    >>> repairorder[0].state
    'done'
