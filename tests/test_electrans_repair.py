# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import unittest
import doctest
import datetime

from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_checker
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from decimal import Decimal
from trytond.modules.company.tests import create_company, set_company
from trytond.modules.electrans_tools.tests.test_tools import (
    create_fiscalyear_and_chart, get_accounts, create_product,
    create_account_category)
from trytond.modules.electrans_tools.tests.test_tools import create_sequence
from trytond.exceptions import UserWarning
from trytond.transaction import Transaction


class RepairTestCase(ModuleTestCase):
    'Test Product Purchase Line Relation module'
    module = 'electrans_repair'

    def create_order(self, repair):
        pool = Pool()
        WizardSaleCreateOrder = pool.get('sale.create_order', type='wizard')
        Sale = pool.get('sale.sale')
        self.assertEqual(len(repair.sale.orders), 0)
        Sale.quote([repair.sale])
        self.assertEqual(repair.sale.state, 'quotation')
        session_id, _, _ = WizardSaleCreateOrder.create()
        wizard_sale_create_order = WizardSaleCreateOrder(session_id)
        wizard_sale_create_order.record = repair.sale
        with Transaction().set_context(active_id=repair.sale.id):
            wizard_sale_create_order.ask_sale.reference = None
            wizard_sale_create_order.ask_sale.attachment = None
            wizard_sale_create_order.transition_start()
            wizard_sale_create_order._execute('create_')
        self.assertEqual(len(repair.sale.orders), 1)

    @with_transaction()
    def test_repair_workflow(self):
        "Test Repair Workflow"
        pool = Pool()
        RepairConfiguration = pool.get('repair.configuration')
        SaleConfig = pool.get('sale.configuration')
        SaleCategory = pool.get('sale.category')
        Country = pool.get('country.country')
        Project = pool.get('work.project')
        ShipmentOutReturn = pool.get('stock.shipment.out.return')
        ShipmentOut = pool.get('stock.shipment.out')
        ShipmentInternal = pool.get('stock.shipment.internal')
        ProductUom = pool.get('product.uom')
        Lang = pool.get('ir.lang')
        Sale = pool.get('sale.sale')
        Party = pool.get('party.party')
        Tax = pool.get('account.tax')
        StockLot = pool.get('stock.lot')
        Repair = pool.get('repair.repair')
        Location = pool.get('stock.location')
        Carrier = pool.get('carrier')
        es, = Lang.search([('code', '=', 'es')])
        Warning = pool.get('res.user.warning')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        # Used Wizards
        WizardRepairCreate = pool.get('repair.create', type='wizard')
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            revenue = accounts.get('revenue')
            expense = accounts.get('expense')
            tax, = Tax.search([], limit=1)
            sale_category = SaleCategory(name='SaleCategory')
            sale_category.save()
            account_category_goods_parent = create_account_category(tax, expense, revenue)
            # Create account category services
            account_category = create_account_category(tax, expense, revenue, "Servicios")
            # Create account category
            account_category_goods = create_account_category(tax, expense, revenue)
            account_category_goods.parent = account_category_goods_parent
            account_category_goods.save()
            # Create repair service product
            template_service, product_service = create_product('Product Repair Service', account_category)
            template_service.salable = True
            template_service.sale_uom = unit.id
            template_service.save()
            # Create Locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            repair_location, = Location.search([('code', '=', 'STO')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = repair_location
            warehouse.save()
            lost_found_location, = Location.search([('type', '=', 'lost_found')], limit=1)
            # Add repair configuration data
            config = RepairConfiguration(1)
            sequence = create_sequence('Repairs', 'Repair Sequence', 'RMA')
            config.repair_sequence = sequence
            config.repair_location = repair_location
            config.lost_location = lost_found_location
            config.sale_title = 'Repair'
            config.max_time_assessment = datetime.timedelta(days=14)
            config.max_time_fixing = datetime.timedelta(days=28)
            config.product_category = account_category_goods_parent
            config.irreparable_assessment_service = product_service
            config.save()
            # Add sale configuration
            config = SaleConfig(1)
            sequence = create_sequence('Offer', 'Sale')
            config.sale_quotation_sequence = sequence
            config.default_in_force_period = datetime.timedelta(days=120)
            config.save()
            # Create Party
            party, = Party.create([{
                'name': 'Party',
                'lang': es,
                'allowed_for_sales': True,
                'addresses': [('create', [{
                        'street': 'Street 1',
                        'city': 'Manresa',
                        'postal_code': '08240',
                        'invoice': True,
                        'delivery': True,
                        }])],
            }])
            self.assertEqual(len(party.addresses), 1)
            # Create project
            project, = Project.create([{
                'number': 'OFR0001',
                'party': party,
                'title': 'REPAIRS',
                'category': sale_category.id,
            }])
            # Create goods product
            template, product = create_product('Product 1', account_category_goods)
            template.salable = True
            template.sale_uom = unit.id
            template.fix_product = template_service.id
            template.save()
            # Create carrier
            Carrier.create([{
                'party': party.id,
                'carrier_product': product_service.id,
            }])
            # Create lot
            lot = StockLot()
            lot.number = 'LOT001'
            lot.product = product
            lot.save()
            # Execute Wizard 3 times to create 3 repairs
            repairs = Repair.search([])
            self.assertEqual(len(repairs), 0)
            for i in range(0, 3):
                session_id, _, _ = WizardRepairCreate.create()
                wizard_repair_create = WizardRepairCreate(session_id)
                wizard_repair_create.start.party = party
                wizard_repair_create.start.delivery_address = party.addresses[0]
                wizard_repair_create.start.invoice_address = party.addresses[0]
                wizard_repair_create.start.contact = None
                wizard_repair_create.start.filename = None
                wizard_repair_create.start.customer_request_document = None
                wizard_repair_create.start.notes_for_customer_shipment = None
                wizard_repair_create.start.origin = 'customer'
                wizard_repair_create.start.project = project
                # Create repair
                repair = Repair()
                repair.product = product
                repair.lot = lot
                repair.unknown_lot = False
                repair.failure_description = None
                wizard_repair_create.start.repair_orders = [repair]
                # Test if a warning is being raised when executing "create"
                # in the wizard and skips the warning at the same time, because
                # adding the code in a with self.assertRaises(UserWarning):
                # block is not possible, as the code is not executed
                try:
                    wizard_repair_create._execute('create_')
                    # if the line above is not raising an UserWarning,
                    # the test will fail
                    self.fail("Repair create wizard warning not raised")
                except UserWarning as warning:
                    # Saves the warning key to avoid the same warning when
                    # executing the "create" button again
                    _, (key, *_) = warning.args
                Warning(user=Transaction().user, name=key).save()
                wizard_repair_create._execute('create_')
            repairs = Repair.search([])
            self.assertEqual(len(repairs), 3)
            self.assertEqual(len([r.sale for r in repairs if r.sale]), 3)
            repair1 = repairs[0]
            repair2 = repairs[1]
            repair3 = repairs[2]
            # Do the incoming_shipment of the repairs and fill fields that will be required
            for rep in repairs:
                self.assertIsNotNone(rep.incoming_shipment)
                ShipmentOutReturn.receive([rep.incoming_shipment])
                self.assertEqual(rep.incoming_shipment.state, 'received')
                ShipmentOutReturn.done([rep.incoming_shipment])
                self.assertEqual(rep.incoming_shipment.state, 'done')
                rep.sale.country = Country(name='Spain')
                rep.sale.category = sale_category.id
                rep.sale.invoice_method = 'shipment'
                rep.sale.save()

            # Test Repair / Irreparable --> Lost Found Shipment
            repair1.assessment = 'irreparable'
            # Check 100% Discount
            repair1.under_warranty = True
            repair1.return_to_customer = False
            repair1.save()
            self.assertEqual(len(repair1.sale.lines), 0)
            Repair.do([repair1])
            self.assertEqual(repair1.state, 'done')
            self.assertEqual(len(repair1.sale.lines), 1)
            # Pass offer to quotation state and create order
            self.create_order(repair1)
            repair_order1 = repair1.sale.orders[0]
            # Process order
            Sale.confirm(repair1.sale.orders)
            self.assertEqual(repair_order1.state, 'confirmed')
            Sale.process(repair1.sale.orders)
            self.assertEqual(repair_order1.state, 'processing')
            # Check Internal Shipment has been created and it appears in the repair form
            self.assertEqual(len(repair_order1.internal_shipments), 1)
            self.assertEqual(isinstance(repair_order1.internal_shipments[0], ShipmentInternal), True)
            self.assertIsNotNone(repair1.lost_and_found_shipment)
            # Check Invoice has been created and not linked to shipment move
            self.assertEqual(len(repair_order1.invoices), 1)
            self.assertEqual(len(repair_order1.invoices[0].lines), 1)
            self.assertEqual(repair_order1.invoices[0].total_amount, Decimal('0.00'))
            self.assertEqual(repair_order1.invoices[0].lines[0].discount, Decimal('1.00'))
            self.assertEqual(len(repair_order1.invoices[0].lines[0].stock_moves), 0)
            # Check Invoice and Shipment states from order
            self.assertEqual(repair_order1.invoice_state, 'waiting')
            self.assertEqual(repair_order1.shipment_state, 'waiting')

            # Test Repair / Irreparable --> Send to customer
            repair2.assessment = 'irreparable'
            repair2.return_to_customer = True
            repair2.save()
            self.assertEqual(len(repair2.sale.lines), 0)
            Repair.do([repair2])
            self.assertEqual(repair2.state, 'done')
            self.assertEqual(len(repair2.sale.lines), 1)
            # Pass offer to quotation state and create order
            self.create_order(repair2)
            repair_order2 = repair2.sale.orders[0]
            # Process order
            Sale.confirm(repair2.sale.orders)
            self.assertEqual(repair_order2.state, 'confirmed')
            Sale.process(repair2.sale.orders)
            self.assertEqual(repair_order2.state, 'processing')
            # Check Customer Shipment has been created, and it appears in the repair form
            self.assertEqual(len(repair_order2.shipments), 1)
            self.assertEqual(isinstance(repair_order2.shipments[0], ShipmentOut), True)
            self.assertIsNotNone(repair2.outgoing_shipment)
            self.assertEqual(repair2.outgoing_shipment.state, repair_order2.shipments[0].state)
            # Check Invoice has been created and not linked to shipment move
            self.assertEqual(len(repair_order2.invoices), 1)
            self.assertEqual(len(repair_order2.invoices[0].lines), 1)
            self.assertEqual(repair_order2.invoices[0].untaxed_amount,
                             repair_order2.invoices[0].lines[0].gross_unit_price)
            self.assertEqual(repair_order2.invoices[0].lines[0].discount, Decimal('0.00'))
            self.assertEqual(len(repair_order2.invoices[0].lines[0].stock_moves), 0)
            # Check Invoice and Shipment states from order
            self.assertEqual(repair_order2.invoice_state, 'waiting')
            self.assertEqual(repair_order2.shipment_state, 'waiting')

            # Test Repair / Reparable
            repair3.assessment = 'reparable'
            repair3.save()
            self.assertEqual(len(repair3.sale.lines), 0)
            Repair.wait_to_fix([repair3])
            self.assertEqual(repair3.state, 'waiting_to_fix')
            self.assertEqual(len(repair3.sale.lines), 1)
            # Pass offer to quotation state and create order
            self.create_order(repair3)
            repair_order3 = repair3.sale.orders[0]
            # Process order
            Sale.confirm(repair3.sale.orders)
            self.assertEqual(repair_order3.state, 'confirmed')
            Sale.process(repair3.sale.orders)
            self.assertEqual(repair_order3.state, 'processing')
            # Check Customer Shipment has been created, and it appears in the repair form
            self.assertEqual(len(repair_order3.shipments), 1)
            self.assertEqual(isinstance(repair_order3.shipments[0], ShipmentOut), True)
            self.assertIsNotNone(repair3.outgoing_shipment)
            self.assertEqual(repair_order3.shipment_state, 'waiting')
            self.assertEqual(repair3.outgoing_shipment.state, repair_order3.shipments[0].state)
            # Check Invoice is created after the customer shipment is done
            self.assertEqual(len(repair_order3.invoices), 0)
            ShipmentOut.pick(repair_order3.shipments)
            self.assertEqual(repair3.outgoing_shipment.state, 'picked')
            ShipmentOut.pack(repair_order3.shipments)
            self.assertEqual(repair3.outgoing_shipment.state, 'packed')
            ShipmentOut.done(repair_order3.shipments)
            self.assertEqual(repair3.outgoing_shipment.state, 'done')
            # Reprocess sale cause when a move is finished the sale is processed
            Sale.process(repair3.sale.orders)
            self.assertEqual(len(repair_order3.invoices), 1)
            # Check Invoice is linked to shipment move
            self.assertEqual(len(repair_order3.invoices[0].lines), 1)
            self.assertEqual(len(repair_order3.invoices[0].lines[0].stock_moves), 1)
            # Check Invoice and Shipment states from order
            self.assertEqual(repair_order3.invoice_state, 'waiting')
            self.assertEqual(repair_order3.shipment_state, 'sent')


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
         RepairTestCase))
    suite.addTests(doctest.DocFileSuite(
        'scenario_reopen_repair.rst',
        tearDown=doctest_teardown, encoding='utf-8',
        checker=doctest_checker,
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
