# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.exceptions import UserWarning
from trytond.i18n import gettext
from trytond.transaction import Transaction

__all__ = ['Sale', 'SaleLine']


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return super(Sale, cls)._get_origin() + ['repair.package']

    @classmethod
    def quote(cls, sales):
        pool = Pool()
        RepairPackage = pool.get('repair.package')
        Warning = Pool().get('res.user.warning')
        for sale in sales:
            if isinstance(sale.origin, RepairPackage):
                if not all(repair.sale_lines for repair in sale.origin.repair_orders):
                    key = 'sale.repair.identifier%s' % (sale.id)
                    if Warning.check(key):
                        raise UserWarning(key, gettext(
                            'electrans_repair.not_all_repairs_assessed'))
        super(Sale, cls).quote(sales)

    def is_done(self):
        pool = Pool()
        Repair = pool.get('repair.repair')
        done = ((self.invoice_state == 'paid' or self.invoice_state == 'none') and
                (self.shipment_state == 'sent' or self.shipment_state == 'none' or
                 all(l.product.type == 'service' and not isinstance(l.origin, Repair)
                     for l in self.lines if l.product)))
        return done if not done else super(Sale, self).is_done()

    @classmethod
    def process(cls, sales):
        def create_shipment_out(move):
            RepairConfiguration = pool.get('repair.configuration')
            config = RepairConfiguration(1)
            shipment = ShipmentOut()
            shipment.customer = line.origin.party.id
            shipment.delivery_address = sale.shipment_address.id
            shipment.company = sale.company.id
            shipment.warehouse = config.repair_location.warehouse.id
            shipment.carrier = sale.carrier.id if sale.carrier else None
            shipment.moves = (list(getattr(shipment, 'moves', [])) + [move])
            # 'planned_date': delivery_date
            shipment.save()
            return shipment

        def create_internal_shipment(line):
            # Create an internal shipment to move the irreparable product
            # to a lost and found location
            pool = Pool()
            RepairConfiguration = pool.get('repair.configuration')
            ShipmentInternal = pool.get('stock.shipment.internal')
            Move = pool.get('stock.move')
            Company = pool.get('company.company')

            conf = RepairConfiguration(1)
            repair_location = conf.repair_location
            from_location = repair_location.id
            to_location = conf.lost_location.id
            related_to = line.origin.number
            company = Transaction().context.get('company')
            # Check if move exist before create
            exist_move = [m for m in line.moves if isinstance(m.shipment, ShipmentInternal)]
            if not exist_move:
                shipment, = ShipmentInternal.create([{
                    'from_location': from_location,
                    'to_location': to_location,
                    'related_to': related_to,
                }])
                move = Move(product=line.origin.product.id,
                            company=company,
                            quantity=1,
                            internal_quantity=1,
                            currency=Company(company).currency.id,
                            from_location=from_location,
                            to_location=to_location,
                            origin=line,
                            lot=line.origin.lot.id,
                            shipment=str(shipment),
                            state='draft')
                move.on_change_product()
                move.save()

        pool = Pool()
        ShipmentOut = pool.get('stock.shipment.out')
        Repair = pool.get('repair.repair')
        shipment = None
        for sale in sales:
            for line in sale.lines:
                if line.origin and isinstance(line.origin, Repair):
                    if (line.origin.assessment in ['not_damaged', 'reparable']
                            or line.origin.return_to_customer):
                        if not line.origin.outgoing_moves:
                            move = line.create_outgoing_move(sale)
                            if shipment:
                                move.shipment = shipment
                                move.save()
                            else:
                                shipment = create_shipment_out(move)
                            ShipmentOut.wait([shipment])
                            ShipmentOut.assign_try([shipment])
                    else:
                        create_internal_shipment(line)
        super(Sale, cls).process(sales)


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    origin = fields.Reference('Origin', selection='get_origin')

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['repair.repair']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
            ('model', 'in', models),
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    def create_outgoing_move(self, sale):
        pool = Pool()
        RepairConfiguration = pool.get('repair.configuration')
        Move = pool.get('stock.move')
        warehouse = RepairConfiguration(1).repair_location.warehouse

        move = Move()
        move.quantity = 1
        move.uom = self.origin.product.default_uom
        move.product = self.origin.product
        move.from_location = warehouse.output_location.id if warehouse and \
            warehouse.output_location else None
        move.to_location = self.origin.party.customer_location.id if \
            self.origin.party.customer_location else None
        move.state = 'draft'
        move.lot = self.origin.lot
        move.origin = self
        # TODO: the move cost should be the same as the incoming move because the product/lot cost do no change while a reparation is done
        move.unit_price = self.origin.product.cost_price
        move.company = sale.company
        move.currency = sale.currency
        move.on_change_product()
        move.save()
        return move

    def _get_invoice_line_quantity(self):
        'Return the quantity that should be invoiced'
        pool = Pool()
        Uom = pool.get('product.uom')
        Repair = pool.get('repair.repair')
        if (self.product and self.product.type == 'service'
                and isinstance(self.origin, Repair)
                and (self.origin.assessment in ['not_damaged', 'reparable'])
                and self.sale.invoice_method == 'shipment'):
            quantity = 0.0
            for move in self.moves:
                if move.state != 'done':
                    continue
                qty = Uom.compute_qty(move.uom, move.quantity, self.unit)
                # Test only against to_location
                # as it is what matters for sale
                dest_type = 'customer'
                if (move.to_location.type == dest_type
                        and move.from_location.type != dest_type):
                    quantity += qty
                elif (move.from_location.type == dest_type
                        and move.to_location.type != dest_type):
                    quantity -= qty
            return quantity
        else:
            return super()._get_invoice_line_quantity()

    @property
    def _move_remaining_quantity(self):
        pool = Pool()
        Uom = pool.get('product.uom')
        Repair = pool.get('repair.repair')
        if (self.product and self.product.type == 'service' and
                isinstance(self.origin, Repair)):
            skip_ids = set(x.id for x in self.moves_ignored)
            quantity = abs(self.quantity)
            for move in self.moves:
                if move.state == 'done' or move.id in skip_ids:
                    quantity -= Uom.compute_qty(
                        move.uom, move.quantity, self.unit)
            return quantity
        else:
            return super()._move_remaining_quantity
