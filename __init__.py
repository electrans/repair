# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import repair
from . import stock
from . import product
from . import project
from . import sale


def register():

    Pool.register(
        product.Template,
        project.Work,
        repair.Repair,
        repair.RepairConfiguration,
        repair.RepairConfigurationRelationType,
        repair.RepairConfigurationSequence,
        repair.RepairCreateStart,
        repair.RepairPackage,
        sale.Sale,
        sale.SaleLine,
        stock.Lot,
        stock.Move,
        stock.ShipmentOutReturn,
        module='electrans_repair', type_='model')
    Pool.register(
        repair.RepairCreate,
        repair.RepairReopen,
        module='electrans_repair', type_='wizard')
