# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields

__all__ = ['Move', 'ShipmentOutReturn']


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    repair_number = fields.Function(
        fields.Char("Repair Number"),
        'get_repair_number')

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['repair.repair']

    def get_repair_number(self, name=None):
        """
        Returns the repair number if the stock move
        belongs to inventory moves
        """
        pool = Pool()
        ShipmentOut = pool.get('stock.shipment.out')
        SaleLine = pool.get('sale.line')
        if (self.to_location == self.shipment.warehouse_output and not self.origin and
                self.lot and isinstance(self.shipment, ShipmentOut)):
            for outgoing_move in self.shipment.outgoing_moves:
                # Origin of the origin sale line of the move will be the repair
                if (isinstance(outgoing_move.origin, SaleLine) and
                        outgoing_move.origin.origin and outgoing_move.lot == self.lot):
                    return outgoing_move.origin.origin.number


class ShipmentOutReturn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    def _get_inventory_move(self, incoming_move):
        pool = Pool()
        Repair = pool.get('repair.repair')
        RepairConfiguration = pool.get('repair.configuration')
        move = super(ShipmentOutReturn, self)._get_inventory_move(
            incoming_move)
        if isinstance(incoming_move.origin, Repair):
            move.to_location = RepairConfiguration(1).repair_location.id
        return move


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'
    repairs = fields.One2Many('repair.repair', 'lot', "Repairs", readonly=True)
