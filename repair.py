# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields, ModelSingleton, Workflow
from trytond.modules.company.model import CompanyValueMixin, CompanyMultiValueMixin
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Id, Not
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateAction, Button
from trytond.modules.account_invoice_contact.invoice import ContactMixin
from trytond.exceptions import UserWarning
from decimal import Decimal
from datetime import datetime
import logging
try:
    from jinja2 import Template as Jinja2Template
    jinja2_loaded = True
except ImportError:
    jinja2_loaded = False
    logging.getLogger('electrans_sale').error(
        'Unable to import jinja2. Install jinja2 package.')

__all__ = ['Repair', 'RepairConfiguration', 'RepairConfigurationSequence', 'RepairPackage']

_DRAFT_STATES = {
    'readonly': Eval('state') != 'draft',
    }
_WAITING_FOR_ASSESSMENT_STATES = {
    'readonly': Eval('state') != 'waiting_for_assessment',
    }
_WAITING_TO_FIX_STATES = {
    'readonly': Eval('state') != 'waiting_to_fix',
    }
_INCONMING_SHIPMENT_ORIGIN = ['stock.shipment.out.return', 'stock.shipment.internal']


class RepairPackage(Workflow, ModelSQL, ModelView, ContactMixin):
    """Repair Package"""
    __name__ = 'repair.package'

    number = fields.Char(
        "Number",
        readonly=True)
    party = fields.Many2One("party.party", "Customer")
    delivery_address = fields.Many2One(
        'party.address', "Delivery address",
        readonly=True)
    invoice_address = fields.Many2One(
        'party.address', "Invoice address",
        domain=[
            ('party', '=', Eval('party')),
            ('invoice', '=', True)],
        states={'invisible': ~Eval('party')},
        depends=['party'])
    contact = fields.Many2One(
        'party.party', 'Contact',
        domain=[('id', 'in', Eval('allowed_invoice_contacts', []))],
        depends=['party', 'allowed_invoice_contacts'])
    _contact_config_name = 'repair.configuration'
    _contact_config_template_field = 'party'
    project = fields.Many2One(
        "work.project", "Project",
        domain=[
            ('party', '=', Eval('party'))],
        depends=['party'])
    repair_orders = fields.One2Many(
        'repair.repair', 'repair_package', "Repair orders",
        readonly=True)
    customer_request_document = fields.Binary(
        'Customer request document', filename='filename')
    filename = fields.Char('Filename')
    notes_for_customer_shipment = fields.Text(
        "Notes",
        help="Notes for customer shipment.")
    # Sales
    sales = fields.Function(
        fields.One2Many('sale.sale', None, "Sales"),
        'get_sales')
    # Logistics
    incoming_shipment = fields.Function(
        fields.Reference("Incoming shipment", selection='get_origin'),
        'get_incoming_shipment')
    state = fields.Function(
        fields.Selection([
            ('draft', "Draft"),
            ('waiting_for_assessment', "Waiting for assessment"),
            ('waiting_to_fix', "Waiting to fix"),
            ('done', "Done")],
            "State", readonly=True),
        'get_state', searcher='search_state')

    @classmethod
    def copy(cls, moves, default=None):
        raise UserError(gettext(
            'electrans_repair.do_not_allow_to_duplicate'))

    @classmethod
    def create(cls, vlist):
        if not Transaction().context.get('created_from_wizard'):
            raise UserError(gettext(
                'electrans_repair.do_not_allow_to_create'))
        return super(RepairPackage, cls).create(vlist)

    def get_sales(self, name=None):
        Sale = Pool().get('sale.sale')
        sales = Sale.search([('origin', '=', str(self))])
        return [sale.id for sale in sales]

    def get_incoming_shipment(self, name=None):
        return str(self.repair_orders[0].incoming_shipment) if self.repair_orders else None

    @fields.depends('invoice_address', 'delivery_address', 'party')
    def on_change_party(self):
        self.delivery_address = None
        self.invoice_address = None
        if self.party:
            self.delivery_address = self.party.address_get(type='delivery')
            invoice_address = self.party.address_get(type='invoice')
            if invoice_address and invoice_address.invoice:
                self.invoice_address = self.party.address_get(type='invoice')

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
            ('model', 'in', models),
        ])
        return [('', '')] + [(m.model, m.name) for m in models]

    @classmethod
    def _get_origin(cls):
        'Model names that use in reference type field'
        return _INCONMING_SHIPMENT_ORIGIN

    def get_state(self, name=None):
        states = [order.state for order in self.repair_orders]
        if 'draft' in states:
            return 'draft'
        if 'waiting_for_assessment' in states:
            return 'waiting_for_assessment'
        if 'waiting_to_fix' in states:
            return 'waiting_to_fix'
        if 'done' in states:
            return 'done'

    @classmethod
    def search_state(cls, name, clause):
        return [('repair_orders.state',) + tuple(clause[1:])]


class Repair(Workflow, ModelSQL, ModelView):
    """Repair"""
    __name__ = 'repair.repair'
    _rec_name = 'number'

    number = fields.Char(
        "Number",
        readonly=True)
    reference = fields.Char("Reference",
        help="The customer reference for the repair order.",
        states=_DRAFT_STATES,
        depends=['state'])
    repair_package = fields.Many2One(
        'repair.package', 'Repair Package',
        domain=[('party', '=', Eval('party'))],
        depends=['party'])
    party = fields.Function(
        fields.Many2One('party.party', 'Customer'),
        'get_repair_package_field', searcher='search_repair_party')
    contact = fields.Function(
        fields.Many2One('party.party', 'Contact'),
        'get_repair_package_field')
    product = fields.Many2One(
        'product.product', "Product",
        domain=[
            ('type', '=', 'goods'),
            ('account_category.parent', 'child_of', Eval('product_category'))],
        states={
            'readonly': Eval('state') != 'draft',
            'required': True,
            },
        depends=['state', 'product_category'])
    lot = fields.Many2One(
        'stock.lot', "Lot",
        domain=[('product', '=', Eval('product'))],
        states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('state') != 'draft',
            },
        depends=['product', 'state'])
    unknown_lot = fields.Boolean(
        "Unknown lot",
        states={
            'readonly': Eval('state') != 'draft',
            'required': ~Eval('lot'),
            },
        depends=['state', 'lot'])
    state = fields.Selection([
        ('draft', "Draft"),
        ('waiting_for_assessment', "Waiting for assessment"),
        ('waiting_to_fix', "Waiting to fix"),
        ('done', "Done")],
        "State", readonly=True)
    product_category = fields.Function(
        fields.Many2One(
            'product.category', 'Product Category'),
        'get_product_category')
    # Sale
    sale = fields.Function(
        fields.Many2One('sale.sale', "Sale"),
        'get_sale')
    sale_state = fields.Function(
        fields.Selection([
            ('', ''),
            ('draft', 'Draft'),
            ('waiting', 'Waiting'),
            ('quotation', 'Quotation'),
            ('waiting_confirmation', 'Waiting confirmation'),
            ('confirmed', 'Confirmed'),
            ('processing', 'Processing'),
            ('done', 'Done'),
            ('cancel', 'Canceled')],
            "Sale state"),
        'get_sale_state')
    sale_lines = fields.One2Many('sale.line', 'origin', 'Sale Lines')
    # Logistics
    incoming_shipment_state = fields.Function(
        fields.Selection([
            ('', ''),
            ('draft', 'Draft'),
            ('waiting', 'Waiting'),
            ('received', 'Received'),
            ('shipped', 'Shipped'),
            ('done', 'Done'),
            ('exception', 'Exception')],
            "Incoming shipment state"),
        'get_incoming_shipment_state')
    customer_return_moves = fields.Function(fields.One2Many(
        'stock.move', None, "Customer Return Moves"),
        'get_customer_return_moves')
    incoming_shipment = fields.Function(
        fields.Reference("Incoming shipment", selection='get_origin'),
        'get_incoming_shipment')
    outgoing_shipment = fields.Function(
        fields.Many2One('stock.shipment.out', "Outgoing shipment"),
        'get_outgoing_shipment')
    outgoing_shipment_state = fields.Function(
        fields.Selection([
            ('', ''),
            ('draft', 'Draft'),
            ('waiting', 'Waiting'),
            ('picked', 'Picked'),
            ('packed', 'Packed'),
            ('assigned', 'Assigned'),
            ('done', 'Done'),
            ('cancelled', 'Cancelled')],
            "Outgoing shipment state"),
        'get_outgoing_shipment_state')
    outgoing_moves = fields.Function(
        fields.One2Many('stock.move', None, "Outgoing Moves"),
        'get_outgoing_moves')
    lost_and_found_shipment = fields.Function(
        fields.Many2One(
            'stock.shipment.internal', "Lost and found shipment",
            states={'invisible': Not(Bool(Eval('lost_and_found_shipment')))}),
        'get_lost_and_found_shipment')
    delivery_date = fields.Function(
        fields.Date('Delivery date'),
        'get_delivery_date')
    delivery_address = fields.Function(
        fields.Many2One('party.address', "Delivery address"),
        'get_repair_package_field')
    notes_for_customer_shipment = fields.Function(
        fields.Text(
            "Notes",
            help="Notes for customer shipment."),
        'get_repair_package_field')
    # Failure
    create_employee = fields.Function(
        fields.Many2One('company.employee', "Employee"),
        'get_create_employee')
    create_date = fields.Function(
        fields.Date("Create date"),
        'get_create_date')
    failure_description = fields.Text(
        "Breakdown description",
        help="Description given by the customer",
        states=_DRAFT_STATES, depends=['state']
        )
    # Assessment
    assessment = fields.Selection([
        ('', ''),
        ('not_damaged', "Not damaged"),
        ('irreparable', "Irreparable"),
        ('reparable', "Reparable")],
        "Assessment",
        states=_WAITING_FOR_ASSESSMENT_STATES, depends=['state'])
    assessment_string = assessment.translated('assessment')
    under_warranty = fields.Boolean(
        "Under warranty",
        states={
            'readonly': Eval('state') != 'waiting_for_assessment',
            'invisible': Eval('assessment') == ''},
        depends=['state', 'assessment'])
    assessment_information = fields.Text(
        "Assessment Information",
        help="Information relative to the assessment",
        states=_WAITING_FOR_ASSESSMENT_STATES, depends=['state'])
    assessment_repairman = fields.Many2One(
        'company.employee', "Assessment Repairman",
        states=_WAITING_FOR_ASSESSMENT_STATES, depends=['state'])
    assessment_date = fields.Date(
        "Assessment Date",
        states=_WAITING_FOR_ASSESSMENT_STATES, depends=['state'])
    assessment_deadline = fields.Date(
        "Assessment Deadline",
        states=_WAITING_FOR_ASSESSMENT_STATES, depends=['state'])
    return_to_customer = fields.Boolean(
        "Return to Customer",
        states={
            'readonly': Eval('sale_state') == 'processing',
            'invisible': Eval('assessment') != 'irreparable'},
        depends=['assessment', 'sale_state'])
    # Fixing
    fixing_information = fields.Text(
        "Fixing Information",
        help="Information relative to the repair",
        states=_WAITING_TO_FIX_STATES, depends=['state'])
    fixing_repairman = fields.Many2One(
        'company.employee', "Repairman",
        states=_WAITING_TO_FIX_STATES, depends=['state'])
    fixing_date = fields.Date(
        "Fixing Date",
        states=_WAITING_TO_FIX_STATES, depends=['state'])
    fixing_deadline = fields.Date(
        "Fixing Deadline",
        states={'readonly': Eval('state') == 'done'},
        depends=['state'])

    @classmethod
    def __setup__(cls):
        super(Repair, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'waiting_for_assessment'),
            ('waiting_for_assessment', 'draft'),
            ('waiting_for_assessment', 'waiting_to_fix'),
            ('waiting_for_assessment', 'done'),
            ('waiting_to_fix', 'done')
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'waiting_for_assessment',
                'depends': ['state'],
                'icon': 'tryton-back'},
            'wait_for_assessment': {
                'invisible': Eval('state') != 'draft',
                'depends': ['state'],
                'icon': 'tryton-forward'},
            'wait_to_fix': {
                'invisible': Eval('state') != 'waiting_for_assessment',
                'readonly': Eval('assessment') != 'reparable',
                'depends': ['state', 'assessment'],
                'icon': 'tryton-forward'},
            'do': {
                'invisible': ~Eval('state').in_(['waiting_for_assessment', 'waiting_to_fix']),
                'readonly': Eval('assessment') == '',
                'depends': ['state'],
                'icon': 'tryton-ok'}})

    @classmethod
    def copy(cls, moves, default=None):
        raise UserError(gettext(
            'electrans_repair.do_not_allow_to_duplicate'))

    @classmethod
    def create(cls, vlist):
        if not Transaction().context.get('created_from_wizard'):
            raise UserError(gettext(
                'electrans_repair.do_not_allow_to_create'))
        return super(Repair, cls).create(vlist)

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_assessment():
        return ''

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, repairs):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting_for_assessment')
    def wait_for_assessment(cls, repairs):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting_to_fix')
    def wait_to_fix(cls, repairs):
        pool = Pool()
        Date = pool.get('ir.date')
        for repair in repairs:
            if not repair.incoming_shipment_state == 'done':
                raise UserError(gettext(
                    'electrans_repair.no_incoming_move',
                    product=repair.product.rec_name))
            if not repair.assessment:
                raise UserError(gettext(
                    'electrans_repair.assessment_required',
                    repair=repair.number))
            if not repair.assessment_repairman:
                repair.assessment_repairman = Transaction().context.get('employee')
            if not repair.assessment_date:
                repair.assessment_date = Date.today()
            if repair.assessment == 'reparable':
                repair.create_quotation_lines()
        cls.save(repairs)

    def create_quotation_lines(self):
        def create_quotation_line(repair, product, under_warranty, quantity):
            sale_line = SaleLine(
                type='line',
                origin=repair,
                product=product,
                quantity=quantity,
                sale=self.sale.id)
            sale_line.taxes = SaleLine.default_taxes()
            sale_line.on_change_product()
            sale_line.on_change_quantity()
            if under_warranty:
                sale_line.discount = Decimal('1.0')
            sale_line.on_change_discount()
            sale_line.save()
            sale_line.on_change_taxes()
        pool = Pool()
        SaleLine = pool.get('sale.line')
        RepairConfiguration = pool.get('repair.configuration')
        Product = pool.get('product.product')
        if self.sale and self.sale.state == 'draft':
            if self.assessment == 'reparable':
                if self.product and self.product.template and self.product.template.fix_product:
                    product = Product.search([('template', '=', self.product.template.fix_product.id)])
                    create_quotation_line(
                        repair=str(self),
                        product=product[0].id,
                        under_warranty=self.under_warranty,
                        quantity=1)
                else:
                    raise UserError(
                        gettext('electrans_repair.no_product_repair_service',
                        product=self.product.rec_name))
            elif self.assessment == 'irreparable':
                # Create a line with irreparable assessment service
                create_quotation_line(
                    repair=str(self),
                    product=RepairConfiguration(1).irreparable_assessment_service.id,
                    under_warranty=self.under_warranty,
                    quantity=1)
            elif self.assessment == 'not_damaged':
                # Create a line with not damaged assessment cost
                create_quotation_line(
                    repair=str(self),
                    product=RepairConfiguration(1).not_damaged_assessment_service.id,
                    under_warranty=self.under_warranty,
                    quantity=1)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def do(cls, repairs):
        Date = Pool().get('ir.date')
        for repair in repairs:
            if not repair.incoming_shipment_state == 'done':
                raise UserError(gettext(
                    'electrans_repair.no_incoming_move',
                    product=repair.product.rec_name))
            if repair.assessment == 'reparable':
                if not repair.sale or not repair.sale.orders or \
                        repair.sale.orders[0].state != 'processing':
                    raise UserError(gettext(
                        'electrans_repair.do_not_allow_to_repair',
                        repair=repair.rec_name))
            if not repair.assessment_repairman:
                repair.assessment_repairman = Transaction().context.get('employee')
            if not repair.assessment_date:
                repair.assessment_date = Date.today()
            if not repair.fixing_repairman:
                repair.fixing_repairman = Transaction().context.get('employee')
            if not repair.fixing_date:
                repair.fixing_date = Date.today()
            if repair.assessment == 'irreparable' or repair.assessment == 'not_damaged':
                repair.create_quotation_lines()
        cls.save(repairs)

    def get_sale(self, name=None):
        if self.sale_lines:
            # Return the first quotation of the linked lines to the repair
            for line in self.sale_lines:
                if line.sale and not line.sale.is_order and line.sale.not_from_presupuestario:
                    return line.sale.id
        elif self.repair_package.sales:
            return self.repair_package.sales[0].id
        return None

    def get_sale_state(self, name=None):
        if self.sale and self.sale.orders and \
                self.sale.orders[0].state == 'draft':
            return 'waiting_confirmation'
        sale = self.sale.orders[0] if self.sale  and self.sale.orders else self.sale
        return sale.state if sale else ""

    def get_incoming_shipment_state(self, name=None):
        return self.incoming_shipment.state if self.incoming_shipment else None

    def get_incoming_shipment(self, name=None):
        for move in self.customer_return_moves:
            if move.from_location.type == 'customer' and move.shipment:
                return str(move.shipment)

    def get_outgoing_shipment_state(self, name=None):
        return self.outgoing_shipment.state if self.outgoing_shipment else None

    def get_outgoing_shipment(self, name=None):
        for move in self.outgoing_moves:
            return move.shipment.id if move.shipment else None

    def get_outgoing_moves(self, name=None):
        Move = Pool().get('stock.move')
        moves = Move.search([
            ('origin.origin', '=', str(self), 'sale.line'),
            ('to_location.type', '=', 'customer')])
        return [move.id for move in moves]

    def get_customer_return_moves(self, name=None):
        Move = Pool().get('stock.move')
        customer_return_moves = Move.search([
            ('origin', '=', str(self)),
            ('from_location.type', '=', 'customer')])
        return customer_return_moves

    def get_lost_and_found_shipment(self, name=None):
        pool = Pool()
        Move = pool.get('stock.move')
        InternalShipment = pool.get('stock.shipment.internal')
        move = Move.search([('origin.origin', '=', str(self), 'sale.line'),
                            ('to_location.type', '=', 'lost_found')], limit=1)
        if move and move[0].shipment and isinstance(move[0].shipment, InternalShipment):
            return move[0].shipment.id

    def get_create_employee(self, name=None):
        pool = Pool()
        User = pool.get('res.user')
        if self.create_uid:
            user = User(self.create_uid)
            if user.employees:
                return user.employees[0].id

    def get_create_date(self, name=None):
        return self.create_date

    def get_repair_package_field(self, name):
        res = getattr(self.repair_package, name) if self.repair_package else None
        if res:
            return res if isinstance(res, str) else res.id

    def get_delivery_date(self, name):
        customer_return_moves = self.customer_return_moves
        if customer_return_moves:
            dates = filter(None, (m.effective_date for m in customer_return_moves if m.state != 'cancelled'))
            return min(dates, default=None)

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
            ('model', 'in', models),
        ])
        return [('', '')] + [(m.model, m.name) for m in models]

    @classmethod
    def _get_origin(cls):
        'Model names that use in reference type field'
        return _INCONMING_SHIPMENT_ORIGIN

    @fields.depends('assessment')
    def on_change_with_return_to_customer(self, name=None):
        return True if self.assessment == 'irreparable' else False

    def get_product_category(self, name=None):
        RepairConfiguration = Pool().get('repair.configuration')
        conf = RepairConfiguration(1)
        return conf.product_category.id if conf.product_category else None

    @classmethod
    def default_product_category(cls):
        RepairConfiguration = Pool().get('repair.configuration')
        conf = RepairConfiguration(1)
        return conf.product_category.id if conf.product_category else None

    @classmethod
    def search_repair_party(cls, name, clause):
        return [('repair_package.party',) + tuple(clause[1:])]


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class RepairConfigurationRelationType(ModelSQL):
    'Repair Configuration - Party relation type'
    __name__ = 'repair.configuration-party.relation.type'

    relation = fields.Many2One('party.relation.type', 'Relation Type',
                               required=True, select=True)
    config = fields.Many2One('repair.configuration', 'Config',
                             required=True, select=True)


class RepairConfiguration(ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Repair Configuration'
    __name__ = 'repair.configuration'

    repair_location = fields.Many2One('stock.location', "Repair location")
    lost_location = fields.Many2One(
        'stock.location', "Lost and found location",
        domain=[('type', '=', 'lost_found')])
    repair_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Repair Sequence", required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('electrans_repair', 'sequence_type_repair')),
        ]))
    relation_types = fields.Many2Many(
        'repair.configuration-party.relation.type', 'config',
        'relation', 'Contact types')
    # This field refers to the repair service(product.product) for products that are irreparable
    irreparable_assessment_service = fields.Many2One(
        'product.product',
        'Irreparable assessment service', domain=[
            ('salable', '=', True),
            ('type', '=', 'service')])
    # This field refers to the repair service(product.product) for products that are not damaged
    not_damaged_assessment_service = fields.Many2One(
        'product.product',
        'Not damaged assessment service', domain=[
            ('salable', '=', True),
            ('type', '=', 'service')])
    sale_title = fields.Char(
        "Sale title",
        required=True,
        translate=True)
    max_time_assessment = fields.TimeDelta("Maximum time to assessment")
    max_time_fixing = fields.TimeDelta("Maximum time to fix")
    product_category = fields.Many2One('product.category', "Product Category")

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'repair_sequence':
            return pool.get('repair.configuration.sequence')
        return super(RepairConfiguration, cls).multivalue_model(field)

    default_repair_sequence = default_func('repair_sequence')


class RepairConfigurationSequence(ModelSQL, CompanyValueMixin):
    "Repair Configuration Sequence"
    __name__ = 'repair.configuration.sequence'

    repair_sequence = fields.Many2One(
        'ir.sequence', "Repair Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=', Id('electrans_repair', 'sequence_type_repair')),
        ], depends=['company'])

    @classmethod
    def default_repair_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('electrans_repair', 'sequence_repair')
        except KeyError:
            return None


class RepairCreateStart(ModelView, ContactMixin):
    'Repair Create Start'
    __name__ = 'repair.create.start'

    party = fields.Many2One(
        'party.party', "Customer",
        required=True)
    project = fields.Many2One(
        'work.project', "Project",
        domain=[('party', '=', Eval('party'))],
        depends=['party'])
    repair_orders = fields.One2Many(
        'repair.repair', None, "Repair orders",
        required=True)
    origin = fields.Selection([
        ('customer', 'Customer'),
        ('company_branch', 'Company Branch')],
        "Origin",
        required=True)
    company_branch = fields.Many2One(
        'company.branch', "Company Branch",
        states={
            'invisible': Eval('origin') != 'company_branch',
            'required': Eval('origin') == 'company_branch'})
    delivery_address = fields.Many2One(
        'party.address', "Delivery address",
        domain=[('party', '=', Eval('party'))],
        depends=['party'])
    invoice_address = fields.Many2One(
        'party.address', "Invoice address",
        domain=[
            ('party', '=', Eval('party')),
            ('invoice', '=', True)],
        depends=['party'])
    contact = fields.Many2One(
        'party.party', 'Contact',
        domain=[
            ('id', 'in', Eval('allowed_invoice_contacts', [])),
        ],
        depends=['allowed_invoice_contacts'])
    _contact_config_name = 'repair.configuration'
    _contact_config_template_field = 'party'
    customer_request_document = fields.Binary(
        'Customer request document',
        filename='filename')
    filename = fields.Char('Filename')
    notes_for_customer_shipment = fields.Text(
        "Notes",
        help="Notes for customer shipment.")

    @staticmethod
    def default_origin():
        return 'customer'

    @fields.depends('invoice_address', 'delivery_address', 'party')
    def on_change_party(self):
        self.delivery_address = None
        self.invoice_address = None
        if self.party:
            self.delivery_address = self.party.address_get(type='delivery')
            invoice_address = self.party.address_get(type='invoice')
            if invoice_address and invoice_address.invoice:
                self.invoice_address = self.party.address_get(type='invoice')


class RepairCreate(Wizard):
    'Repair Create'
    __name__ = 'repair.create'
    start = StateView('repair.create.start',
                      'electrans_repair.repair_create_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Create', 'create_', 'tryton-ok', default=True)])
    create_ = StateAction('stock.act_shipment_out_return_form')

    def do_create_(self, action):
        def create_move(from_location, to_location, lot):
            Move = Pool().get('stock.move')
            move = Move(product=repair.product.id if repair and repair.product else None,
                        company=company,
                        quantity=1,
                        unit_price=repair.product.cost_price if repair and repair.product else 0.00,
                        internal_quantity=1,
                        currency=Company(company).currency.id,
                        from_location=from_location,
                        to_location=to_location,
                        origin=str(new_repair),
                        lot=lot,
                        shipment=str(shipment),
                        state='draft')
            move.on_change_product()
            move.save()
            return move

        pool = Pool()
        Repair = pool.get('repair.repair')
        RepairPackage = pool.get('repair.package')
        RepairConfiguration = pool.get('repair.configuration')
        ShipmentOutReturn = pool.get('stock.shipment.out.return')
        ShipmentInternal = pool.get('stock.shipment.internal')
        Company = pool.get('company.company')
        Sale = pool.get('sale.sale')
        Date = pool.get('ir.date')
        Lot = pool.get('stock.lot')
        Warning = pool.get('res.user.warning')
        config = RepairConfiguration(1)

        # Warning shown after clicking "create" button in the wizard to ensure
        # the user that everything is correct
        key = 'create_%s' % ','.join([
            str(r.product.id) for r in self.start.repair_orders])
        if Warning.check(key):
            raise UserWarning(key, gettext(
                'electrans_repair.create_repair_order',
                product_lot=",\n".join(
                    [f"{r.product.rec_name}\n"
                     f"LOT: {r.lot.rec_name if r.lot else 'Unknown'}"
                     for r in self.start.repair_orders])))

        customer = self.start.party if self.start.party else None
        delivery_address = self.start.delivery_address.id if self.start.delivery_address else None
        invoice_address = self.start.invoice_address.id if self.start.invoice_address else None
        company = Transaction().context.get('company')
        contact = self.start.contact.id if self.start.contact else None
        project = self.start.project.id if self.start.project else None
        repair_location = config.repair_location
        number = config.repair_sequence.get()
        # create repair package
        with Transaction().set_context(created_from_wizard=True):
            repair_package, = RepairPackage.create([{
                'number': number,
                'party': customer,
                'delivery_address': delivery_address,
                'invoice_address': invoice_address,
                'filename': self.start.filename,
                'customer_request_document': self.start.customer_request_document,
                'contact': contact,
                'notes_for_customer_shipment': self.start.notes_for_customer_shipment,
                'project': project}])
        # create sale
        sale = Sale()
        sale.project = project
        sale.company = company
        sale.warehouse = repair_location.warehouse.id if repair_location and repair_location.warehouse else None
        sale.state = 'draft'
        sale.party = customer
        sale.on_change_party()
        sale.not_from_presupuestario = True
        sale.sale_date = Date.today()
        sale.origin = str(repair_package)
        sale.invoice_address = invoice_address
        sale.shipment_address = delivery_address
        with Transaction().set_context(language=customer.lang.code):
            # re-browse the instance with the proper context to get translations if exits
            if RepairConfiguration(1).sale_title:
                template = Jinja2Template(RepairConfiguration(1).sale_title)
                sale.description = template.render({'record': repair_package})
        sale.save()
        # create shipment
        if self.start.origin == 'customer':
            warehouse = repair_location.warehouse
            shipment, = ShipmentOutReturn.create([{
                'customer': customer.id,
                'company': company,
                'warehouse': warehouse.id,
                'delivery_address': delivery_address}])
        elif self.start.origin == 'company_branch':
            warehouse = self.start.company_branch.warehouse
            shipment, = ShipmentInternal.create([{
                'company': company,
                'from_location': repair_location.warehouse.storage_location.id,
                'to_location': repair_location.id}])
        cont = 1
        new_repairs = []
        for repair in self.start.repair_orders:
            repair_number = number + str(cont).zfill(2)
            if repair.unknown_lot:
                prefix = repair.product.template.lot_sequence.prefix if \
                    repair.product.template and \
                    repair.product.template.lot_sequence and \
                    repair.product.template.lot_sequence.prefix else ''
                lot, = Lot.create([{
                        'number': prefix + repair_number.replace('MA', ''),
                        'product': repair.product.id}])
            else:
                lot = repair.lot.id if repair.lot else None
            # create repairs
            with Transaction().set_context(created_from_wizard=True):
                new_repair, = Repair.create([{
                    'number': repair_number,
                    'repair_package': repair_package.id,
                    'failure_description': repair.failure_description,
                    'product': repair.product.id if repair.product else None,
                    'lot': lot,
                    'assessment_deadline': Date.today() + config.max_time_assessment,
                    'fixing_deadline': Date.today() + config.max_time_fixing,
                    'state': 'draft'}])
            new_repairs.append(new_repair)
            cont += 1
            # create moves
            if self.start.origin == 'customer':
                create_move(customer.customer_location.id if customer and customer.customer_location else None,
                            warehouse.input_location.id if warehouse and warehouse.input_location else None,
                            lot)
            else:
                create_move(warehouse.storage_location.id if warehouse and warehouse.storage_location else None,
                            repair_location.repair_location.id,
                            lot)
        Repair.wait_for_assessment(new_repairs)
        data = {'res_id': shipment.id}
        action['views'].reverse()
        return action, data


class RepairReopen(Wizard):
    """Repair Reopen"""
    __name__ = 'repair.reopen'

    start_state = 'reopen'
    reopen = StateAction('electrans_repair.wizard_repair_reopen')

    def do_reopen(self, action):
        """
        This function goes through all the records, notes the id of the sales lines that will be eliminated and returns
        the fields of the repair order to null
        """
        pool = Pool()
        SaleLine = pool.get('sale.line')
        Repair = pool.get('repair.repair')

        lines = []

        for record in self.records:
            if record.state != 'waiting_to_fix' and record.state != 'done' or record.sale_state != 'draft':
                raise UserError(gettext('electrans_repair.wrong_repair_order',
                                        repair=record.rec_name))
            lines.extend([sale_line for sale_line in record.sale_lines])

        Repair.write(self.records, {
                'assessment': '',
                'assessment_information': None,
                'assessment_repairman': None,
                'assessment_date': None,
                'state': 'waiting_for_assessment'
                })

        SaleLine.delete(lines)
